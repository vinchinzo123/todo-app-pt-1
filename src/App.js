import React, { useState } from "react";
import todosList from "./todos.json";

const App = () => {
  const [todos, setTodos] = useState(todosList);
  const [input, setInput] = useState("");

  const handelKeyDown = (e) => {
    if (e.key === "Enter") {
      let newIndex = todos.length;
      let newId = newIndex + 1;

      setTodos([
        ...todos,
        { userId: 1, id: newId, title: input, completed: false },
      ]);
      setInput(() => "");
    }
  };

  const toggleCompleted = (index) => {
    //this function had tricky syntax
    setTodos(
      todos.map((todo, i) => {
        if (index === i) {
          return { ...todo, completed: !todo.completed };
        } else {
          return todo;
        }
      })
    );
  };
  const deleteTodoItem = (id) => {
    let notDeleteTodoList = todos.filter((todo) => {
      if (todo.id !== id) {
        return todo;
      }
    });
    setTodos(notDeleteTodoList);
  };
  const clearComplete = () => {
    let notCompleted = todos.filter((todo) => !todo.completed);
    setTodos(notCompleted);
  };

  const handleOnChange = (e) => {
    e.persist();
    setInput(() => e.target.value);
  };
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          autofocus
          value={input}
          onChange={handleOnChange}
          onKeyDown={handelKeyDown}
        />
      </header>
      <TodoList
        todos={todos}
        deleteTodoItem={deleteTodoItem}
        toggleCompleted={toggleCompleted}
      />
      <footer className="footer">
        <span className="todo-count">
          <strong>{todos.length}</strong> item(s) left
        </span>
        <button onClick={clearComplete} className="clear-completed">
          Clear completed
        </button>
      </footer>
    </section>
  );
};

const TodoItem = (props) => {
  let textDecoration = "none";
  if (props.completed) textDecoration = "line-through";
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          onChange={() => props.toggleCompleted(props.index)}
          className="toggle"
          type="checkbox"
          checked={props.completed}
        />
        <label style={{ textDecorationLine: textDecoration }}>
          {props.title}
        </label>
        <button
          onClick={() => props.deleteTodoItem(props.id)}
          className="destroy"
        />
      </div>
    </li>
  );
};

const TodoList = (props) => {
  const [completed, setCompleted] = useState(props.completed);

  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo, index) => (
          <TodoItem
            key={todo.title}
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            deleteTodoItem={props.deleteTodoItem}
            index={index}
            toggleCompleted={props.toggleCompleted}
          />
        ))}
      </ul>
    </section>
  );
};

export default App;
